package com.aka;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public abstract class BaseActivity extends AppCompatActivity
    implements BaseAsyncTask.DataRequestCallback {
    protected BaseAsyncTask asyncTask;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        ButterKnife.bind(this);
    }

    protected void requestData(int type, String... params) {
        asyncTask = new BaseAsyncTask(this, type, this);
        asyncTask.execute(params);
    }

    @Override
    protected void onDestroy() {
        if(asyncTask != null)
            asyncTask.cancel(true);
        super.onDestroy();
    }

    public void addFragmentWithBackStack(int resId, BaseFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(resId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void addFragmentWithoutBackStack(int resId, BaseFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(resId, fragment);
        transaction.commit();
    }

    public boolean backStackFragment() {
        getSupportFragmentManager().popBackStack();
        return getSupportFragmentManager().getBackStackEntryCount() <= 1;
    }

}
