package com.aka.utils;

import android.content.Context;
import android.content.Intent;

import com.sony.atmfinder.GuideActivity;
import com.sony.atmfinder.MainActivity;

/**
 * Created by cuonghv on 11/5/15.
 */
public class ActivityHelper {

    public static void openHome(Context context) {
        context.startActivity(
            new Intent(context, MainActivity.class)
        );
    }

    public static void openGuide(Context context) {
        context.startActivity(
            new Intent(context, GuideActivity.class)
        );
    }
}
