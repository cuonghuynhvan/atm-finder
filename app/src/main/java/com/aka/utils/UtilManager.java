package com.aka.utils;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public class UtilManager {
    private static UtilManager mInstance;

    private Context mContext;

    private UtilManager() {

    }

    public static UtilManager getInstance() {
        if(mInstance == null)
            mInstance = new UtilManager();

        return mInstance;
    }

    public void init(Context context) {
        mContext = context;
    }

    public String getIMEI() {
        TelephonyManager mngr = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return mngr.getDeviceId();
    }
}
