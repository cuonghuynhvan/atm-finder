package com.aka.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by cuonghv on 11/6/15.
 */
public class MoneyUtils {

    public static String formatMoney(double money, int fraction) {
        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("vi", "VN"));
        format.setMaximumFractionDigits(fraction);
        format.setMinimumFractionDigits(0);
        return format.format(money).replace(" ₫", "");
    }
}
