package com.aka;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.aka.model.BaseModel;
import com.aka.model.ErrorModel;

/**
 * Created by cuong.huynh on 11/5/2015.
 */

public class BaseAsyncTask extends AsyncTask<String, Void, BaseModel> {

    public interface DataRequestCallback {
        void onRequestResult(int type, BaseModel data);
        BaseModel onRequestData(int type, String[] params); ;
    }

    private DataRequestCallback mCallback;
    private int mType;
    private Context mContext;

    public BaseAsyncTask(Context context, int type, DataRequestCallback callback) {
        mType = type;
        mCallback = callback;
        mContext = context;
    }

    @Override
    protected BaseModel doInBackground(String... params) {
        return mCallback.onRequestData(mType, params);
    }

    @Override
    protected void onPostExecute(BaseModel t) {
        super.onPostExecute(t);

        if(t instanceof ErrorModel) {
            //handel error here.
            Toast.makeText(mContext, ((ErrorModel) t).Destscription, Toast.LENGTH_SHORT).show();
        } else
            mCallback.onRequestResult(mType, t);
    }
}