package com.aka;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * Created by cuonghv on 11/5/15.
 */
public abstract class BaseFragment extends Fragment
    implements BaseAsyncTask.DataRequestCallback {

    protected Activity activity;
    protected View rootView;

    protected BaseAsyncTask asyncTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutResId(), container, false);
        ButterKnife.bind(this, rootView);
        initView(rootView);
        return rootView;
    }

    protected abstract int getLayoutResId();
    protected abstract void initView(View rootView);

    protected void requestData(int type, String... params) {
        asyncTask = new BaseAsyncTask(activity, type, this);
        asyncTask.execute(params);
    }

    @Override
    public void onDestroy() {
        if (asyncTask != null)
            asyncTask.cancel(true);
        super.onDestroy();
    }

}
