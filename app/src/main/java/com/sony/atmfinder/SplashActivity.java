package com.sony.atmfinder;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.aka.utils.ActivityHelper;
import com.aka.utils.Constant;

public class SplashActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivityHelper.openGuide(SplashActivity.this);
                finish();
            }
        }, Constant.SPLASH_TIME);
    }

}
