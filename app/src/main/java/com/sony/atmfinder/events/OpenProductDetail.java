package com.sony.atmfinder.events;

import com.sony.atmfinder.models.ProductModel;

/**
 * Created by cuonghv on 11/6/15.
 */
public class OpenProductDetail {
    public ProductModel model;

    public OpenProductDetail(ProductModel model) {
        this.model = model;
    }
}
