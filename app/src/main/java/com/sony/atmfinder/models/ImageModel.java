package com.sony.atmfinder.models;

import com.aka.model.BaseModel;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public class ImageModel extends BaseModel {
//    "ImageUrl":"https://sonycenter.sony.com.vn/Data/Sites/1/Product/336/zx2-3.jpeg",
//    "ThumbnailUrl":"https://sonycenter.sony.com.vn/Data/Sites/1/Product/336/thumbs/zx2-3.jpeg",
//    "Title":""

    public String ImageUrl;
    public String ThumbnailUrl;
    public String Title;
}
