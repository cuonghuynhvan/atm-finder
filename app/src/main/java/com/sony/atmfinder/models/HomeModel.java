package com.sony.atmfinder.models;

import com.aka.model.BaseModel;
import com.aka.model.ErrorModel;

import java.util.List;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public class HomeModel extends BaseModel {
    public List<ProductModel> ListProductHighlightKey;

    public List<ProductModel> ListProductFeature;

    public List<ProductModel> ListProductNew;

    public List<ProductModel> ListProductHot;

    public List<CategoryModel> ListProductCategoryHot;

    public List<ErrorModel> RequestInfo;

}

