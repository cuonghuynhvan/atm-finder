package com.sony.atmfinder.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.aka.model.BaseModel;

public class PromotionModel extends BaseModel {
    private static final String PREF_FILE = "promotionpref";

//    {
//        "PromotionCodes": [
//        {
//            "PromotionCode": "MB_3AE26D4BAB89230FBDB3E8B2365ACA6D"
//        }
//        ],
//        "PromotionInfo": [
//        {
//            "Name": "Mobile",
//                "PromotionDetail": "Giảm 60%",
//                "FromTime": "/Date(1420099200000)/",
//                "ToTime": "/Date(1422604800000)/"
//        },
//        {
//            "Name": "Mobile",
//                "PromotionDetail": "giảm 65%",
//                "FromTime": "/Date(1420185600000)/",
//                "ToTime": "/Date(1425110400000)/"
//        }
//        ],
//        "RequestInfo": [
//        {
//            "ResultId": "1",
//             "Destscription": "OK"
//        }
//        ]
//    }

    public PromotionCodeModel[] PromotionCodes;

    public void savePromotionCode(Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor e = sp.edit();
        e.putString("keycode", PromotionCodes[0].PromotionCode);
        e.commit();
    }

    public static PromotionModel loadFromPref(Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_FILE, 0);

        String code = sp.getString("keycode", null);

        if(code != null) {
            PromotionModel pm = new PromotionModel();
            pm.PromotionCodes = new PromotionCodeModel[1];
            pm.PromotionCodes[0] = new PromotionCodeModel();
            pm.PromotionCodes[0].PromotionCode = code;

            return pm;
        }
        else
            return null;

    }
}
