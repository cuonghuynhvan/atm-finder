package com.sony.atmfinder.models;

import com.aka.model.BaseModel;

public class SonyCenterModel extends BaseModel {
//    {
//        "ListSonyCenter":[
//        {
//            "SonyCenterId":1,
//                "Name":"Sony Center Union Square",
//                "Address":"B1-14, TTTM Union Square, 171 Đồng Khởi, P. Bến Nghé, Quận 1, TP.Hồ Chí Minh",
//                "Latitude":10.776417,
//                "Longitude":6.702389
//        },
//        {
//            "SonyCenterId":2,
//                "Name":"Sony Center",
//                "Address":"23 Trương Định, P.5, Q.3, TP HCM",
//                "Latitude":10.666563987731934,
//                "Longitude":106.54546356201172
//        },
//        {
//            "SonyCenterId":3,
//                "Name":"Sony Center 3",
//                "Address":"18 Trần Quang Khải, P.ĐaKao, Q.1, TP HCM",
//                "Latitude":10.566565,
//                "Longitude":106.806565
//        },
//        {
//            "SonyCenterId":4,
//                "Name":"Hai Ba Trung",
//                "Address":"12 Hai Ba Trung, Q.1, TP.HCM",
//                "Latitude":0,
//                "Longitude":0
//        }
//        ],
//        "RequestInfo":[
//        {
//            "ResultId":"1",
//                "Destscription":"OK"
//        }
//        ]
//    }
    public String SonyCenterId;
    public String Name;
    public String Address;
    public String Latitude;
    public String Longitude;

    public float distance;
}
