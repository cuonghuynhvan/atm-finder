package com.sony.atmfinder.models;

import com.aka.model.BaseModel;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public class ProductModel extends BaseModel {
//    {
//        "ProductId":647,
//        "ProductName":"Máy chụp ảnh ILCE-7RM2/Full Frame 42.4MP (body onl",
//        "Model":"ILCE-7RM2",
//        "Price":59990000.0000,
//        "Photo":"https://sonycenter.sony.com.vn/Data/Sites/1/Product/417/thumbs/ilcer-7rm2-1.jpeg",
//        "ThumbnailUrl":"https://sonycenter.sony.com.vn/Data/Sites/1/Product/417/thumbs/ilcer-7rm2-1.jpeg",
//        "PhotoCaption":null
//    }

    public String ProductId;
    public String ProductName;
    public String Model;
    public double Price;
    public String Photo;
    public String ThumbnailUrl;
    public String PhotoCaption;

    public String Descriptions;
    public int Stock;
    public String PromotionPrice;
}
