package com.sony.atmfinder.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.aka.model.BaseModel;
import com.google.gson.Gson;

public class PromoterListModel extends BaseModel {
    private static final String PREF_FILE = "promoterpref";

    public String mRawJson;
    public long cachingTime;
    public boolean promoted = false;
    public PromoterModel[] ListPromoter;

    public void savePromoterList(Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor e = sp.edit();
        e.putLong("cachingTime", System.currentTimeMillis());
        e.putString("rawJson", mRawJson);
        e.commit();
    }

    public static PromoterListModel loadFromPref(Context ctx){
        SharedPreferences sp = ctx.getSharedPreferences(PREF_FILE, 0);

        boolean promoted = sp.getBoolean("promoted", false);
        long time = sp.getLong("cachingTime", 0);

        if(time != 0) {
            String rawJson = sp.getString("rawJson", null);

            PromoterListModel pm = new Gson().fromJson(rawJson, PromoterListModel.class);
            pm.mRawJson = rawJson;
            pm.cachingTime = time;
            pm.promoted = promoted;

            return pm;
        }
        else
            return null;

    }

    public static void promoteDone(Context ctx) {
        SharedPreferences sp = ctx.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor e = sp.edit();
        e.putBoolean("promoted", true);
        e.commit();
    }
}
