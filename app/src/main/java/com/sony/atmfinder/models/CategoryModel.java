package com.sony.atmfinder.models;

import com.aka.model.BaseModel;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public class CategoryModel extends BaseModel {
//    {
//        "CateId":63,
//        "CateName":"Dàn âm thanh",
//        "Priority":0
//    },

    public String CateId;
    public String CateName;
    public int Priority;
}
