package com.sony.atmfinder.models;

import com.aka.model.BaseModel;
import com.aka.model.ErrorModel;

import java.util.List;

/**
 * Created by cuong.huynh on 11/5/2015.
 */
public class ProductDetailModel extends BaseModel {
    public List<ProductModel> ProductInfo;
    public List<ImageModel> ProductImages;
    public List<ErrorModel> RequestInfo;

}

