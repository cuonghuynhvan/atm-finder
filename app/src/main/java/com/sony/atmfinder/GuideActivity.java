package com.sony.atmfinder;

import android.os.Bundle;
import android.view.View;

import com.aka.BaseActivity;
import com.aka.model.BaseModel;
import com.aka.utils.ActivityHelper;

import butterknife.OnClick;

public class GuideActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);
    }

    @OnClick(R.id.find_around_button)
    public void onFindArroundClicked(View v) {
        ActivityHelper.openHome(this);
    }

    @Override
    public void onRequestResult(int type, BaseModel data) {

    }

    @Override
    public BaseModel onRequestData(int type, String[] params) {
        return null;
    }
}
