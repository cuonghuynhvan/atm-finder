package com.sony.atmfinder;

import android.os.Bundle;

import com.aka.BaseActivity;
import com.aka.model.BaseModel;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onRequestResult(int type, BaseModel data) {

    }

    @Override
    public BaseModel onRequestData(int type, String[] params) {
        return null;
    }
}
