package com.sony.atmfinder.widgets;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sony.atmfinder.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by cuonghv on 11/5/15.
 */
public class HomeWidget {
    @Bind(R.id.title_text)
    TextView tvTitle;

    @Bind(R.id.recyclerView)
    RecyclerView rvList;

    private RecyclerView.Adapter mAdapter;

    public HomeWidget(View view, int titleResId) {
        ButterKnife.bind(this, view);

        tvTitle.setText(titleResId);
        initRecyclerView();
    }

    protected void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvList.getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvList.setLayoutManager(layoutManager);
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        mAdapter = adapter;
        rvList.setAdapter(mAdapter);
    }
}
