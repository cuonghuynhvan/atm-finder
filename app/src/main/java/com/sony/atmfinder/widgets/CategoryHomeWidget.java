package com.sony.atmfinder.widgets;

import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

/**
 * Created by cuonghv on 11/6/15.
 */
public class CategoryHomeWidget extends HomeWidget {
    public CategoryHomeWidget(View view, int titleResId) {
        super(view, titleResId);
    }

    @Override
    protected void initRecyclerView() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(rvList.getContext(),
            LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(layoutManager);
        rvList.setNestedScrollingEnabled(false);
    }
}
